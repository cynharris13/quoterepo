package com.example.nerdquotes

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Quote application.
 *
 * @constructor Create empty Quote application
 */
@HiltAndroidApp
class QuoteApplication : Application()
