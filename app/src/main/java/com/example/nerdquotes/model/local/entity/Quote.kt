package com.example.nerdquotes.model.local.entity

/**
 * Quote entity.
 *
 * @property author
 * @property en
 * @property id
 * @constructor Create empty Quote
 */
data class Quote(
    val author: String,
    val en: String,
    val id: String
)
