package com.example.nerdquotes.model.remote

import com.example.nerdquotes.model.dto.QuoteDTO
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Service to fetch Quotes.
 *
 * @constructor Create empty Quote service
 */
interface QuoteService {
    // BASE -> https://programming-quotes-api.herokuapp.com/
    // GET -> Quotes/
    // QUERY -> ?count=1
    @GET("Quotes/")
    suspend fun getQuote(@Query("count") count: Int): List<QuoteDTO>
}
