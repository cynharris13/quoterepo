package com.example.nerdquotes.model.dto

import kotlinx.serialization.Serializable

@Serializable
data class QuoteDTO(
    val author: String,
    val en: String,
    val id: String
)
