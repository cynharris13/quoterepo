package com.example.nerdquotes.model

import com.example.nerdquotes.model.local.entity.Quote
import com.example.nerdquotes.model.remote.QuoteService
import javax.inject.Inject

/**
 * Quote repository.
 *
 * @constructor Create empty [QuoteRepo]
 */
class QuoteRepo @Inject constructor(private val service: QuoteService) {
    /**
     * Fetch quotes.
     *
     * @param count number of quotes
     * @return [List] of [Quote] where size is count
     */
    suspend fun getQuotes(count: Int): List<Quote> {
        val quoteDTOs = service.getQuote(count)
        return quoteDTOs.map { Quote(it.author, it.en, it.id) }
    }
}
