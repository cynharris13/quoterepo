package com.example.nerdquotes.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nerdquotes.model.QuoteRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Quote view model.
 *
 * @property repo
 * @constructor Create empty Quote view model
 */
@HiltViewModel
class QuoteViewModel @Inject constructor(private val repo: QuoteRepo) : ViewModel() {
    private val _state = MutableStateFlow(QuoteState())
    val state get() = _state.asStateFlow()

    /**
     * Fetch quotes.
     *
     */
    fun fetchQuotes() = viewModelScope.launch {
        _state.update { it.copy(isLoading = true) }
        val quotes = repo.getQuotes(QUOTE_COUNT)
        _state.update { it.copy(isLoading = false, quotes = quotes) }
    }

    companion object {
        private const val QUOTE_COUNT = 10
    }
}
