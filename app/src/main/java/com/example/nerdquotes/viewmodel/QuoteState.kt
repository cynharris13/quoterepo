package com.example.nerdquotes.viewmodel

import com.example.nerdquotes.model.local.entity.Quote

/**
 * Data class to hold quotes for view.
 *
 * @property quotes nerd quotes to be displayed
 * @property isLoading true if currently fetching the quotes, else false
 * @constructor Create instance of [QuoteState]
 */
data class QuoteState(
    val quotes: List<Quote> = emptyList(),
    val isLoading: Boolean = false
)
