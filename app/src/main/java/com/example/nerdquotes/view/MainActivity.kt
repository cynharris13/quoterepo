package com.example.nerdquotes.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import com.example.nerdquotes.model.local.entity.Quote
import com.example.nerdquotes.ui.theme.NerdQuotesTheme
import com.example.nerdquotes.viewmodel.QuoteViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main activity of the application.
 *
 * @constructor Create instance of [MainActivity]
 */
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val quoteViewModel by viewModels<QuoteViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NerdQuotesTheme {
                val state by quoteViewModel.state.collectAsState()
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    LazyColumn {
                        items(items = state.quotes, key = { it.id }) { quote: Quote ->
                            Text(text = quote.en)
                        }
                        item {
                            Button(onClick = quoteViewModel::fetchQuotes) {
                                Text(text = "Fetch")
                            }
                        }
                    }
                }
            }
        }
    }
}
